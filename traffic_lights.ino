#define DEBUG

const int A_R = 2; // автомобильный, красный
const int A_Y = 3; // автомобильный, жёлтый
const int A_G = 4; // автомобильный, зелёный

const int W_R = 5; // пешеходный, красный
const int W_G = 6; // пешеходный, зелёный

const int INDICATOR = 7; // индикатор цикла
const int BUTTON = 8; // кнопка (вход)

bool cycleEnabled = false;
int stageIndex = 0;

enum tristate {OFF, BLINKED, ON};

struct stage {
  tristate indicator;
  tristate a_r;
  tristate a_y;
  tristate a_g;
  tristate w_r;
  tristate w_g;
  unsigned long duration;
};

const int STAGE_COUNT = 7;

const stage STAGES[STAGE_COUNT] = {
  { // Ожидание нажатия
    OFF, // индикатор
    OFF, OFF, ON, // автомобильный светофор
    ON, OFF, // пешеходный светофор
    10  // длительность
  },
  { // от нажатия до истечения минимального времени между циклами
    BLINKED, // индикатор
    OFF, OFF, ON, // автомобильный светофор
    ON, OFF, // пешеходный светофор
    10  // длительность
  },
  { // Авто: заморгал зелёный
    ON, // индикатор
    OFF, OFF, BLINKED, // автомобильный светофор
    ON, OFF, // пешеходный светофор
    3  // длительность
  },
  { // Авто: жёлтый
    ON, // индикатор
    OFF, ON, OFF, // автомобильный светофор
    ON, OFF, // пешеходный светофор
    3  // длительность
  },
  { // Авто: красный, пешеход: зелёный
    ON, // индикатор
    ON, OFF, OFF, // автомобильный светофор
    OFF, ON, // пешеходный светофор
    7  // длительность
  },
  { // Пешеход: заморгал зелёный
    ON, // индикатор
    ON, OFF, OFF, // автомобильный светофор
    OFF, BLINKED, // пешеходный светофор
    3  // длительность
  },
  { // Пешеход: красный, Авто: красно-жёлтый
    ON, // индикатор
    ON, ON, OFF, // автомобильный светофор
    ON, OFF, // пешеходный светофор
    3  // длительность
  },
};

void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  #endif

  pinMode(A_R, OUTPUT);
  pinMode(A_Y, OUTPUT);
  pinMode(A_G, OUTPUT);

  pinMode(W_R, OUTPUT);
  pinMode(W_G, OUTPUT);

  pinMode(INDICATOR, OUTPUT);

  pinMode(BUTTON, INPUT_PULLUP);
}

unsigned long stageStartTime = 0;
bool isButtonPressed = false;
bool prevBtnState = false;

void readButton() {
  bool current = !digitalRead(BUTTON);
  isButtonPressed = !prevBtnState && current;
  prevBtnState = current;
};

bool doStep() {
  return millis() >= (stageStartTime + 1000 * STAGES[stageIndex].duration);
};

bool calcCurrentState(tristate state) {
  switch (state) {
    case OFF: return LOW;
    case ON: return HIGH;
    default: {
      return (millis() - stageStartTime) / 500 % 2;
    }
  }
}

void setOutputs(int stageIndex) {
  stage it = STAGES[stageIndex];
  digitalWrite(A_R, calcCurrentState(it.a_r));
  digitalWrite(A_Y, calcCurrentState(it.a_y));
  digitalWrite(A_G, calcCurrentState(it.a_g));

  digitalWrite(W_R, calcCurrentState(it.w_r));
  digitalWrite(W_G, calcCurrentState(it.w_g));

  digitalWrite(INDICATOR, calcCurrentState(it.indicator));
}

#ifdef DEBUG
void printState() {
  Serial.print("enabled: ");
  Serial.print(cycleEnabled);
  Serial.print("\t stage: ");
  Serial.print(stageIndex);
  Serial.print("\t");

  Serial.print("Stage duration (ms): ");
  Serial.print(1000 * STAGES[stageIndex].duration);
  Serial.print("\t");
  Serial.print("Time to end (ms): ");
  Serial.print( max(0, static_cast<long>((stageStartTime + 1000 * STAGES[stageIndex].duration) - millis())) );
  Serial.println();
}
#endif

void loop() {
  readButton();
  #ifdef DEBUG
  printState();
  #endif

  if (isButtonPressed && !cycleEnabled) {
    #ifdef DEBUG
    Serial.println("START");
    #endif
    // Начало цикла
    stageIndex++;
    cycleEnabled = true;
    // stageStartTime остаётся неизменным
  }
  else if (cycleEnabled && doStep()) {
    #ifdef DEBUG
    Serial.println("DO STEP");
    #endif
    stageStartTime = millis();
    stageIndex++;
    if (stageIndex == STAGE_COUNT) {
      // Окончание цикла
      stageIndex = 0;
      cycleEnabled = 0;
    }
  }
  setOutputs(stageIndex);
}
